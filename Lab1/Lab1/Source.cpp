#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <random>

struct bit
{
	unsigned short a : 1;
};

void dec2bin(uint8_t number, std::vector<unsigned short> *output) {
	unsigned short temp = number;
	std::vector<unsigned short> tempVector;
	try
	{
		while (true)
		{
			if (temp == 0)
			{
				tempVector.push_back(0);
				break;
			}
			if (temp == 1)
			{
				tempVector.push_back(1);
				break;
			}
			tempVector.push_back(temp % 2);
			temp /= 2;
		}

		if (tempVector.size() != 8)
		{
			for (size_t i = tempVector.size(); i < 8; i++)
			{
				tempVector.push_back(0);
			}
		}

		std::reverse(tempVector.begin(), tempVector.end());
		for (auto a : tempVector) output->push_back(a);
	}
	catch (const std::bad_alloc& bad_alloc_ex)
	{
		std::cout << "\n\tAlokacja pamieci nie powiodla sie! " << bad_alloc_ex.what() << std::endl;
	}
	
}

void parityBit(bit *tempBit, std::vector<unsigned short> srcVector, std::vector<unsigned short> *dstVector) {
	try
	{
		tempBit->a = 0;
		*dstVector = srcVector;
		for (auto a : *dstVector)
		{
			tempBit->a += a;
		}
		for (size_t i = 0; i < 7; i++)
		{
			dstVector->push_back(0);
		}
		dstVector->push_back(tempBit->a);

	}
	catch (const std::bad_alloc& bad_alloc_ex)
	{
		std::cout << "\n\tAlokacja pamieci nie powiodla sie! " << bad_alloc_ex.what() << std::endl;
	}
}

void moduloChecksum(const char* buffer, const int bufferSize, int* checksum) {
	for (size_t i = 0; i != bufferSize; i++)
	{
		*checksum += (buffer[i]);
	}
	*checksum %= 0xff;
}
void moduloChecksumForVectors(std::vector<unsigned short> buffer, const int bufferSize, int* checksum) {
	int j = 7;
	unsigned short sum = 0;
	*checksum = 0;
	for (size_t i = 0; i < buffer.size(); i++)
	{
		if (buffer[i] == 1)
		{
			sum += pow(2, j);
		}
		if (j < 0)
		{
			*checksum += sum;
			sum = 0;
			j = 7;
		}
		j--;
	}
	if (sum != 0)
	{
		*checksum += sum;
	}
	
	*checksum %= 0xff;
}

void crcN(const unsigned short nBits, const std::vector<unsigned short> polynomial, std::vector<unsigned short> bits, std::vector<unsigned short> &bitsWithCRC) {
	bit oneBit;
	try
	{
		bitsWithCRC = bits;
		for (size_t i = 0; i < nBits; i++)
		{
			bits.push_back(0);
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << "\n\tWystapil blad kopiowania danych! " << ex.what() << std::endl;
	}

	for (size_t i = 0; i < (bits.size() - nBits); i++)
	{
		if (bits.at(i) == 1)
		{
			for (size_t j = 0; j != nBits + 1; j++)
			{
				oneBit.a = polynomial.at(j);
				oneBit.a += bits.at(i + j);
				bits.at(i+j) = oneBit.a;
			}
		}
		else
		{
			continue;
		}
	}
	try
	{
		for (size_t i = bits.size() - nBits; i < bits.size(); i++)
		{
			bitsWithCRC.push_back(bits.at(i));
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << "\n\tWystapil blad kopiowania danych! " << ex.what() << std::endl;
	}
}
void checkCRC(const unsigned short nBits, const std::vector<unsigned short> polynomial, std::vector<unsigned short> bitsWithCRC) {
	bit oneBit;
	
	for (size_t i = 0; i < (bitsWithCRC.size() - nBits); i++)
	{
		if (bitsWithCRC.at(i) == 1)
		{
			for (size_t j = 0; j != nBits + 1; j++)
			{
				oneBit.a = polynomial.at(j);
				oneBit.a += bitsWithCRC.at(i + j);
				bitsWithCRC.at(i + j) = oneBit.a;
			}
		}
		else
		{
			continue;
		}
	}
	try
	{
		std::vector<unsigned short> compareResult(bitsWithCRC.size(), 0);
		if (compareResult == bitsWithCRC) std::cout << "Wektory rowne!" << std::endl;
	}
	catch (const std::exception& ex)
	{
		std::cout << "\n\tWystapil blad kopiowania danych! " << ex.what() << std::endl;
	}
}

void hello() {
	std::cout << "\n\tSieci teleinformatyczne - lab 1" << std::endl
		<< "\t\tAdrian Kastrau\n" << std::endl;
}

inline unsigned short calculateErrors(size_t vectorSize, float errorPercentage) {
	return std::floor(vectorSize * errorPercentage);
}

void makeErrors(std::vector<unsigned short> *dstVector, size_t errors, bool repeatErrors, std::vector<unsigned short> *drawnNumbers) {
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(0, dstVector->size() - 1);

	std::vector<unsigned short> drawnNumbersTemp;
	bit oneBit;

	size_t index = 0;

	while (errors > 0)
	{
		if (drawnNumbers->size() == dstVector->size() && errors > 0)
		{
			std::cout << "\nNie da sie wygenerowac bledow bez powtorzen!" << std::endl;
			system("pause");
			exit(1);
		}
		if (repeatErrors)
		{
			index = dist(mt);
			while ((std::find(drawnNumbersTemp.begin(), drawnNumbersTemp.end(), index) != drawnNumbersTemp.end()))
			{
				index = dist(mt);
			}
			oneBit.a = dstVector->at(index);
			oneBit.a += 1;
			dstVector->at(index) = oneBit.a;
			drawnNumbersTemp.push_back(index);
		}
		else
		{
			index = dist(mt);

			while ((std::find(drawnNumbers->begin(), drawnNumbers->end(), index) != drawnNumbers->end()))
			{
				index = dist(mt);
			}
			oneBit.a = dstVector->at(index);
			oneBit.a += 1;
			dstVector->at(index) = oneBit.a;
			drawnNumbers->push_back(index);
		}
		errors--;
	}

}

int main(void) {
	hello();
	system("Title ST - lab 1");

	float errorPercentage = 0.0f;
	short crcBitsCount = 0;
	bool repeatErrors = false;

	std::cout << "Wprowadz liczbe bitow dla CRC: ";

	while (!(std::cin >> crcBitsCount) || crcBitsCount <= 0)
	{
		std::cin.clear();
		std::cin.ignore();

		std::cout << "\nWprowadz liczbe bitow dla CRC: ";
	}

	std::cout << "Wprowadz liczbe procentowa bledow: ";

	while (!(std::cin >> errorPercentage) || errorPercentage < 0)
	{
		std::cin.clear();
		std::cin.ignore();

		std::cout << "\nWprowadz liczbe procentowa bledow: ";
	}

	errorPercentage /= 100;

	std::cout << "Bledy maja sie powtarzac? [0/1]: ";

	while (!(std::cin >> repeatErrors))
	{
		std::cin.clear();
		std::cin.ignore();

		std::cout << "\nBledy maja sie powtarzac? [0/1]: ";
	}

	std::cout << "Wprowadz wielomian: ";
	bool polynomialBit = false;
	size_t remainingBits = crcBitsCount + 1;
	std::vector<unsigned short> polynomial;

	while (std::cin >> polynomialBit || remainingBits > 0)
	{
		if (std::cin.good())
		{
			polynomial.push_back(polynomialBit);
			remainingBits--; 
			std::cin.clear();
			std::cin.ignore();
			if (!remainingBits)
			{
				break;
			}
		}
		else
		{
			std::cin.clear();
			std::cin.ignore();

			std::cout << "\nWprowadz wielomian: ";
		}
	}

	bit tempBit;
	std::ifstream file;
	file.exceptions(std::ifstream::badbit | std::ifstream::failbit);
	unsigned int fileSize = 0;
	char *buffer = NULL;
	try
	{
		file.open("testowy.txt", std::ios::binary | std::ios::ate);
		fileSize = file.tellg();
		file.seekg(0, std::ios::beg);
		try
		{
			buffer = new char[fileSize + 1];
			memset(buffer, 0, fileSize + 1);
		}
		catch (const std::bad_alloc& bad_alloc_ex)
		{
			std::cout << "\n\tWystapil blad - alokacja pamieci: " << bad_alloc_ex.what() << std::endl;
		}
		file.read(buffer, fileSize);
		file.close();
	}
	catch (const std::ifstream::failure& a) 
	{
		std::cout << "\n\tWystapil blad: " << a.what() << std::endl;
		system("pause");
		return 1;
	}

	//std::cout.write(buffer, fileSize);

	int checksum = 0;
	moduloChecksum(buffer, fileSize, &checksum);

	std::vector<unsigned short> checksumBits;
	std::vector<unsigned short> checksumBitsWithErrors;

	std::vector<unsigned short> bits;
	std::vector<unsigned short> bitsWithErrors;
	std::vector<unsigned short> drawnNumbers;


	for (size_t i = 0; i < fileSize; i++)
	{
		dec2bin(buffer[i], &bits);
	}
	
	size_t errors = calculateErrors(bits.size(), errorPercentage);

	if (errors * 5 > bits.size() && !repeatErrors)
	{
		std::cout << "\nNie da sie wygenerowac bledow bez powtorzen!" << std::endl;
		system("pause");
		exit(1);
	}
	std::ofstream output("wyniki.txt");
	output << "Wyniki dzialania programu:" << std::endl << std::endl
		<< "CRC" << crcBitsCount << ": ";
	for (auto v : polynomial) output << v;
	output << std::endl <<
		"Ilosc procentowa bledow: " << errorPercentage * 100 << "%" << std::endl
		<< "Czy powtarzac bledy? " << repeatErrors << std::endl;
	for (size_t i = 0; i < 5; i++)
	{
		try
		{
			bitsWithErrors = bits;
			makeErrors(&bitsWithErrors, errors, repeatErrors, &drawnNumbers);
			
			output << "Runda: " << i + 1 << std::endl
				<< "Dane przed uszkodzeniem: ";
			for (auto v : bits) output << v;
			output << std::endl
				<< "Dane po uszkodzeniu: ";
			for (auto v : bitsWithErrors) output << v;
			output << std::endl
				<< "Rozmiar danych (w bitach): " << bits.size() << std::endl;

			parityBit(&tempBit, bitsWithErrors, &checksumBitsWithErrors);
			parityBit(&tempBit, bits, &checksumBits);

			output << "Bit parzystosci - wyniki" << std::endl
				<< "=================================" << std::endl
				<< "Dane przed uszkodzeniem: ";
			for (auto v : checksumBits) output << v;
			output << std::endl
				<< "Dane po uszkodzeniu: ";
			for (auto v : checksumBitsWithErrors) output << v;
			output << std::endl
				<< "Czy dane sa zgodne: ";
			if (checksumBits == checksumBitsWithErrors) output << 1;
			else output << 0;
			output << std::endl
				<< "Suma modulo - wyniki" << std::endl
				<< "=========================" << std::endl;
			checksumBits = bits;
			dec2bin(checksum, &checksumBits);
			checksumBitsWithErrors = bitsWithErrors;
			moduloChecksumForVectors(checksumBitsWithErrors, fileSize, &checksum);
			dec2bin(checksum, &checksumBitsWithErrors);
			output << "Dane przed uszkodzeniem: ";
			for (auto v : checksumBits) output << v;
			output << std::endl
				<< "Dane po uszkodzeniu: ";
			for (auto v : checksumBitsWithErrors) output << v;
			output << std::endl
				<< "Czy dane sa zgodne: ";
			if (checksumBits == checksumBitsWithErrors) output << 1;
			else output << 0;
			output << std::endl
				<< "CRC - wyniki" << std::endl
				<< "=========================" << std::endl;
			checksumBits = bits;
			checksumBitsWithErrors = bitsWithErrors;
			crcN(crcBitsCount, polynomial, bits, checksumBits);
			crcN(crcBitsCount, polynomial, bitsWithErrors, checksumBitsWithErrors);
			output << "Dane przed uszkodzeniem: ";
			for (auto v : checksumBits) output << v;
			output << std::endl
				<< "Dane po uszkodzeniu: ";
			for (auto v : checksumBitsWithErrors) output << v;
			output << std::endl
				<< "Czy dane sa zgodne: ";
			if (checksumBits == checksumBitsWithErrors) output << 1;
			else output << 0;
			output << std::endl;

		}
		catch (const std::exception& ex)
		{
			std::cout << "Wystapil blad! " << ex.what() << std::endl;
		}
	}
	output.close();
	checksumBits.clear();
	checksumBitsWithErrors.clear();
	bits.clear();
	bitsWithErrors.clear();
	drawnNumbers.clear();
	polynomial.clear();

	delete[] buffer;
	system("pause");
}