#include "Exceptions.h"



Exceptions::Exceptions()
{
}


Exceptions::~Exceptions()
{
}

std::string Exceptions::outOfMemoryException()
{
	return "Out of memory exception: ";
}

std::string Exceptions::badPortException()
{
	return  "Bad source/destination port.";
}

std::string Exceptions::streamIOException()
{
	return "I/O exception: ";
}

std::string Exceptions::badPacketLengthException()
{
	return "Bad packet length!";
}
