#pragma once
#include <iostream>
class Exceptions
{
public:
	Exceptions();
	~Exceptions();
	static std::string outOfMemoryException();
	static std::string badPortException();
	static std::string streamIOException();
	static std::string badPacketLengthException();
};

