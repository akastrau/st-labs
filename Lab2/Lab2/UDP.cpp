#include "UDP.h"
#include <iostream>
#include "Exceptions.h"

UDP::UDP()
{
}

UDP::~UDP()
{
	data.~vector();
	dataSize = 0;
}

void UDP::sendData(char * data, size_t dataSize, std::ofstream & file)
{
	this->data = dec2bin(reinterpret_cast<uint8_t*>(data), dataSize);
	this->dataSize = this->data.size();
	size_t numberOfPackets = 0;
	if (this->dataSize % 32 != 0)
	{
		numberOfPackets = (this->dataSize / 32) + 1;
	}
	else
	{
		numberOfPackets = this->dataSize / 32;
	}
	

	packetHeader.set_source_port(1100);
	packetHeader.set_destination_port(2122);

	if (numberOfPackets == 1)
	{
		packetHeader.set_data(this->data);
		packetHeader.writePacket(file);
	}
	else
	{
		std::vector<bool> tempVector;
		try
		{
			tempVector.reserve(32);
		}
		catch (std::exception& bad_allocation_exception)
		{
			std::cerr << Exceptions::outOfMemoryException << bad_allocation_exception.what()
				<< std::endl;
		}
		size_t j = 0;
		for (size_t i = 0; i < numberOfPackets; i++)
		{
			size_t remainingBits = 0;

			if (i == numberOfPackets - 1)
			{
				remainingBits = ((this->dataSize) - (32 * i)) + (32 * i);
			}
			else
			{
				remainingBits = (32 * i) + 32;
			}
			while (j < remainingBits)
			{
				tempVector.push_back(this->data[j]);
				j++;
			}
			packetHeader.set_data(tempVector);
			packetHeader.writePacket(file);
			tempVector.clear();
		}
		tempVector.~vector();
		file.close();
	}
}

void UDP::readData(std::ifstream & file)
{
	packetHeader.readPacket(file);
}


std::vector<bool> UDP::dec2bin(uint8_t* buffer, size_t bufferSize)
{
	try
	{
		data.reserve(bufferSize * 8);
		data.clear();
	}
	catch (const std::bad_alloc& bad_alloc_exception)
	{
		std::cerr << Exceptions::outOfMemoryException << bad_alloc_exception.what()
			<< std::endl;
	}
	
	std::vector<bool> finalVector; //for return
	std::vector<bool> tempVector; //for 1uint8_t
	for (size_t i = 0; i < bufferSize; i++)
	{
		uint8_t temp = i;
		try
		{
			while (true)
			{
				if (temp == 0)
				{
					tempVector.push_back(0);
					break;
				}
				if (temp == 1)
				{
					tempVector.push_back(1);
					break;
				}
				tempVector.push_back(temp % 2);
				temp /= 2;
			}

			if (tempVector.size() != 8)
			{
				for (size_t j = tempVector.size(); j < 8; j++)
				{
					tempVector.push_back(0);
				}
			}
			reverse(tempVector.begin(), tempVector.end());
			for (auto v : tempVector) finalVector.push_back(v);
			tempVector.clear();
		}
		catch (const std::bad_alloc& bad_alloc_exception)
		{
			std::cerr << Exceptions::outOfMemoryException << bad_alloc_exception.what() 
				<< std::endl;
		}
	}
	tempVector.~vector();
	return finalVector;
}

