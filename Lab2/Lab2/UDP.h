#pragma once
#include <vector>
#include <fstream>

#include "UDPHeader.h"

class UDP
{
public:
	UDP();
	~UDP();
	void sendData(char* data, size_t dataSize, std::ofstream& file);
	void readData(std::ifstream& file);


private:
	std::vector<bool> data;
	UDPHeader packetHeader;
	size_t dataSize = 0;

	std::vector<bool> dec2bin(uint8_t* buffer, size_t bufferSize);

};