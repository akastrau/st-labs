#include "UDPHeader.h"
#include "Exceptions.h"
#include <iterator>

UDPHeader::UDPHeader()
{
	sourcePort = 0;
	destinationPort = 0;
	packetLength = 8;
	packetChecksum = 0;
	data.resize(32);
	data.clear();
}

UDPHeader::~UDPHeader()
{
	data.~vector();
}

void UDPHeader::set_data(const std::vector<bool>& data)
{
	try
	{
		this->data = data;
	}
	catch (std::exception& bad_allocation_exception)
	{
		std::cerr << Exceptions::outOfMemoryException << bad_allocation_exception.what()
			<< std::endl;
	}
}

uint16_t UDPHeader::source_port() const
{
	return sourcePort;
}

void UDPHeader::set_source_port(uint16_t source_port)
{
	if (source_port <= 0 || source_port > 65535)
	{
		throw Exceptions::badPortException();
	}
	sourcePort = source_port;
}

uint16_t UDPHeader::destination_port() const
{
	return destinationPort;
}

void UDPHeader::set_destination_port(uint16_t destination_port)
{
	if (destination_port <= 0 || destination_port > 65535)
	{
		throw Exceptions::badPortException();
	}
	destinationPort = destination_port;
}

uint16_t UDPHeader::packet_length() const
{
	return packetLength;
}

void UDPHeader::set_packet_length(uint16_t packet_length)
{
	if (packet_length < 8 || packet_length > 96)
	{
		throw Exceptions::badPacketLengthException();
	}
	packetLength = packet_length;
}

void UDPHeader::destroyDataBuffer()
{
	data.clear();
}

void UDPHeader::dec2bin(uint16_t dec, std::vector<bool> &dstVector) const
{
	std::vector<bool> tempVector; //for 1uint8_t
	
	unsigned short temp = dec;
	try
	{
		while (true)
		{
			if (temp == 0)
			{
				tempVector.push_back(0);
				break;
			}
			if (temp == 1)
			{
				tempVector.push_back(1);
				break;
			}
			tempVector.push_back(temp % 2);
			temp /= 2;
		}

		if (tempVector.size() != 8)
		{
			for (size_t j = tempVector.size(); j < 16; j++)
			{
				tempVector.push_back(0);
			}
		}
		reverse(tempVector.begin(), tempVector.end());
		for (auto v : tempVector) dstVector.push_back(v);
		tempVector.clear();
	}
	catch (const std::bad_alloc& bad_alloc_exception)
	{
		std::cerr << Exceptions::outOfMemoryException << bad_alloc_exception.what()
			<< std::endl;
	}

	tempVector.~vector();

}

uint16_t UDPHeader::binToDec(std::vector<bool> srcVector, size_t begin, size_t end) const
{
	if (end - begin != 16)
	{
		return 0;
	}
	size_t j = 15;
	uint16_t sum = 0;
	for (size_t i = begin; i < end; i++)
	{
		if (srcVector[i] == 1)
		{
			sum += pow(2, j);
		}
		j--;
	}
	return sum;
}

void UDPHeader::writePacket(std::ofstream & file)
{
	std::vector<bool> packetToWrite;
	dec2bin(sourcePort, packetToWrite);
	dec2bin(destinationPort, packetToWrite);
	packetLength = data.size() + 0x40;
	dec2bin(packetLength, packetToWrite);
	dec2bin(packetChecksum, packetToWrite);
	
	packetToWrite.insert(packetToWrite.end(), data.begin(), data.end());
	for (auto v : packetToWrite) file << v;

	packetToWrite.~vector();
	destroyDataBuffer();
}

void UDPHeader::readPacket(std::ifstream & file)
{
	std::vector<bool> buffer{ std::istream_iterator<bool>{file}, {} };

	size_t numberOfPackets = buffer.size() / 0x60;
	if (buffer.size() % 0x60 != 0)
	{
		numberOfPackets++;
	}
	std::ofstream output("output.txt", std::ios::binary);
	size_t index = 0;
	for (size_t i = 0; i < numberOfPackets; i++)
	{
		uint16_t data = 0;
		data = binToDec(buffer, index, index + 16);
		index += 16;
		sourcePort = data;
		data = binToDec(buffer, index, index + 16);
		index += 16;
		destinationPort = data;
		data = binToDec(buffer, index, index + 16);
		index += 16;
		packetLength = data;
		data = binToDec(buffer, index, index + 16);
		index += 16;
		packetChecksum = data;
		size_t dataSize = packetLength - 64;
		system("pause");
		//read data as char[]
	}
}


