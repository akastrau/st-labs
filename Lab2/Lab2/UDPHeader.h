#pragma once
#include <vector>
#include <fstream>

class UDPHeader
{
public:
	UDPHeader();
	~UDPHeader();

private:
	uint16_t sourcePort = 0;
	uint16_t destinationPort = 0;
	uint16_t packetLength = 8;
	uint16_t packetChecksum = 0;
	std::vector<bool> data; //MAX 32 BIT

public:
	void set_data(const std::vector<bool>& data);
	uint16_t source_port() const;
	void set_source_port(uint16_t source_port);
	uint16_t destination_port() const;
	void set_destination_port(uint16_t destination_port);
	uint16_t packet_length() const;
	void set_packet_length(uint16_t packet_length);;
	void writePacket(std::ofstream& file);
	void readPacket(std::ifstream& file);

private:
	void destroyDataBuffer();
	void dec2bin(uint16_t dec, std::vector<bool> &dstVector) const;
	uint16_t binToDec(std::vector<bool> srcVector, size_t begin = 0, size_t end = 0) const;
};